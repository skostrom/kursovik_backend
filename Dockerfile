FROM alpine:latest as build

RUN apk add openjdk8
RUN apk add gradle

RUN mkdir /backend
COPY ./dev-school-app-master /backend
WORKDIR /backend/
RUN gradle build

RUN ls -l /backend/build/libs/dev-school-app-1.0-SNAPSHOT.jar
#ENTRYPOINT ["java","-jar","/backend/build/libs/dev-school-app-1.0-SNAPSHOT.jar"]
RUN echo "----=====++++++ FINNISH BUILD STAGE 1 ! +++++=====----"


 
FROM openjdk:8 as readybackend
EXPOSE 80/tcp
EXPOSE 8080/tcp
EXPOSE 8081/tcp
RUN mkdir /app
COPY --from=build /backend/build/libs/dev-school-app-1.0-SNAPSHOT.jar /app
RUN ls -l /app/
ENTRYPOINT ["java","-jar","/app/dev-school-app-1.0-SNAPSHOT.jar"]
RUN echo "----=====++++++ FINNISH BUILD STAGE 2 ! +++++=====----"
